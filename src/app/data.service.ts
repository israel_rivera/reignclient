import { Injectable } from '@angular/core';
import { Data } from './model/model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiURL = 'http://localhost:8080/api';  // URL to web api
  constructor(private http: HttpClient) { }

  public get()
  {
    return this.http.get<any[]>(`${this.apiURL}/data/`);
  }

  public update(obj:Data)
  {
    return this.http.put<Data>(`${this.apiURL}/data/` + obj._id,obj);
  }

  
}
