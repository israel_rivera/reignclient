import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Data } from '../model/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  lData: Data[] = [];
  displayedColumns: string[] = ['title', 'created_at', 'action'];
  constructor(private service: DataService) { }

  ngOnInit() {
    this.loadData();

  }

  loadData() {
    var arr: any[] = [];
    this.lData = [];
    this.service.get().subscribe(res => {
      res["data"].forEach(element => {
        if ((element.title != null || element.story_title != null))
          arr.push(element);
      });
      this.lData = arr.sort((val1, val2) => { return <any>(val2.created_at) - <any>(val1.created_at) });
    });
  }
  removeElement(obj: Data ) {
    if (confirm('are you sure ?')) {
      obj.Status = "0";
      this.service.update(obj).subscribe(res => {
        this.loadData();
      });
    }
  }

  gotoPage(obj) {
    if (obj.story_url != null || obj.url != null)
      window.open((obj.story_url != null) ? obj.story_url : obj.url);
    else
      alert("url not found!");
  }

}
