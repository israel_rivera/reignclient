export class Data {
    _id : string;
    created_at: Date;
    title: String;
    url: String;
    author: String;
    points: String;
    story_text: String;
    comment_text: String;
    num_comments: String;
    story_id: Number;
    story_title: String;
    story_url: String;
    parent_id: Number;
    objectID : Number;
    Status : String;
  }